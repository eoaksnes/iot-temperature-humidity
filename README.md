# IoT Workshop 2

## Step 1 - Create a new project on Firebase

Go to Firebase console and create new project: https://console.firebase.google.com/?pli=1

You need an gmail account for create Firebase project!

## Step 2 - Arduino Inital Setup

We need to install the firebase-arduino library, which can be found in Firebase's Github. 
Follow the steps to install. https://github.com/firebase/firebase-arduino

## Step 3 - Update variables in Sketch

You will need to change these following variables to use with your project in Firebase:

* FIREBASE_HOST: Fill in the URL of your bank in Firebase
* FIREBASE_AUTH: Here your bank's Secret must be placed in Firebase
* WIFI_SSID and WIFI_PASSWORD: you must inform the data to connect in the WiFi of your house.

## Step 4 - Start Web application

To deploy/start the application to Firebase we need to install firebase-tools, 
which is a command-line tool for managing the database and hosting applications within the Firebase platform. 
This tool is based on NodeJS, so we need to install this first. 
Download [NodeJS](https://nodejs.org/en/download) from the official website and install it on your preferred operating system. 
This will also install the package manager NPM. 

With Node and NPM installed just run the command:

```
npm install -g firebase-tools
```

At the end of the installation, run the following command and enter your Firebase credentials:

```
firebase login
```

The firebase-tools also has a built-in server:

```
firebase serve
```

This will start a web server in the public folder on port 5000 by default.

Now you just have to deploy the application in Firebase hosting. 

```
firebase deploy
```

Just run this command that will send the files from the public folder,
there and the link will be displayed to access your online application.

## Configure

Update `src/config.js`.