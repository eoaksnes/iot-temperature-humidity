/*
   Rui Santos
   Complete Project Details http://randomnerdtutorials.com
*/

#include <WiFi.h>
#include "DHT.h"
#include <TimeLib.h>
#include <ArduinoJson.h>
#include "WiFiClientSecure.h"
#include <NTPClient.h>

// Uncomment one of the lines below for whatever DHT sensor type you're using!
#define DHTTYPE DHT11   // DHT 11
//#define DHTTYPE DHT21   // DHT 21 (AM2301)
//#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321

// Replace with your network credentials
const char* ssid     = "";
const char* password = "";
#define PUBLISH_INTERVAL 1000*60*5
const String FIREBASE_HOST = "blinky-6f3e6.firebaseio.com";
const String FIREBASE_AUTH = "";

WiFiServer server(80);

// DHT Sensor
const int DHTPin = 16;

// Initialize DHT sensor.
DHT dht(DHTPin, DHTTYPE);

// Temporary variables
static char celsiusTemp[7];
static char fahrenheitTemp[7];
static char humidityTemp[7];

// Client variables
char linebuf[80];
int charcount = 0;

bool publishNewState = true;

void publish() {
  publishNewState = true;
}

WiFiUDP ntpUDP;

// You can specify the time server pool and the offset (in seconds, can be
// changed later with setTimeOffset() ). Additionaly you can specify the
// update interval (in milliseconds, can be changed using setUpdateInterval() ).
NTPClient timeClient(ntpUDP, "europe.pool.ntp.org", 3600, 60000);

void setup() {
  // initialize the DHT sensor
  dht.begin();

  //Initialize serial and wait for port to open:
  Serial.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  // We start by connecting to a WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  // attempt to connect to Wifi network:
  while (WiFi.status() != WL_CONNECTED) {
    // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  server.begin();
  timeClient.begin();
}

char* string2char(String command){
    if(command.length()!=0){
        char *p = const_cast<char*>(command.c_str());
        return p;
    }
}

void loop() {
  timeClient.update();

  int epoch = timeClient.getEpochTime();
  
  Serial.println("Publish new state:");

  // Get the DHT sensor data
  float humidity = dht.readHumidity();
  float temperature = dht.readTemperature();

  if (!isnan(humidity) && !isnan(temperature)) {
    // Use WiFiClient class to create TCP connections
    WiFiClientSecure client;
    const int httpPort = 443;
    if (!client.connect(string2char(FIREBASE_HOST), httpPort)) {
      Serial.println("connection failed");
      return;
    }

    String url = "/monitoring.json?auth=" + FIREBASE_AUTH;

    Serial.print("Requesting URL: ");
    Serial.print(url);

    // your JSON payload
    StaticJsonBuffer<200> jsonBuffer;
    JsonObject& root = jsonBuffer.createObject();
    root["temperature"] = temperature;
    root["humidity"] = humidity;
    root["epochInSeconds"] = epoch;

    String postData;
    root.printTo(postData);
    
    client.println("POST " + url + " HTTP/1.1");
    client.println("Host: " + FIREBASE_HOST);
    client.println("User-Agent: Arduino/unowifi");
    client.println("Connection: close");
    client.print("Content-Length: ");

    client.println(postData.length());// number of bytes in the payload
    client.println();// important need an empty line here
    client.println(postData);// the payload

    unsigned long timeout = millis();
    while (client.available() == 0) {
      if (millis() - timeout > 50000) {
        Serial.println(">>> Client Timeout !");
        client.stop();
        return;
      }
    }

    // Read all the lines of the reply from server and print them to Serial
    while (client.available()) {
      String line = client.readStringUntil('\r');
      Serial.print(line);
    }

    Serial.println();
    Serial.println("closing connection");

    // close the connection:
    client.stop();
    Serial.println("client disconnected");
  }
  
  // give the web browser time to receive the data
  delay(1000*10);
}
