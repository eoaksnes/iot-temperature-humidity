import React from 'react'
import PropTypes from 'prop-types'
import Navbar from '../../containers/Navbar/Navbar'

export const CoreLayout = ({children}) => (
    <div>
        <Navbar />
        <div className='container'>{children}</div>
    </div>
)

CoreLayout.propTypes = {
    children: PropTypes.element.isRequired
}

export default CoreLayout
