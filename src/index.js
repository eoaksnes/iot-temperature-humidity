import React from 'react'
import ReactDOM from 'react-dom'
import createStore from './store/createStore'
//import { version } from '../package.json'
//import { env } from './config'
import 'normalize.css'

// Window Variables
// ------------------------------------
//window.version = version
//window.env = env

// Store Initialization
// ------------------------------------
const initialState = window.___INITIAL_STATE__ || {
        firebase: {authError: null}
    }
const store = createStore(initialState)

// Render Setup
// ------------------------------------
const MOUNT_NODE = document.getElementById('root')

let render = () => {
    const App = require('./containers/App').default
    const routes = require('./routes/index').default(store)

    ReactDOM.render(<App store={store} routes={routes}/>, MOUNT_NODE)
}

render()