import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import Theme from '../../../theme'
import {
    firebaseConnect,
    isLoaded,
    populate // for populated list
} from 'react-redux-firebase'
import CircularProgress from 'material-ui/CircularProgress'
import Paper from 'material-ui/Paper'
import Subheader from 'material-ui/Subheader'
import './HomeContainer.css'
import {compose} from 'redux'
import {Charts, ChartContainer, ChartRow, YAxis, LineChart} from "react-timeseries-charts";
import {TimeSeries} from "pondjs"; //TimeRange
import Snackbar from 'material-ui/Snackbar'
import Moment from 'moment'

const populates = [{child: 'owner', root: 'users'}]

class Home extends Component {
    static propTypes = {
        monitoring: PropTypes.oneOfType([
            PropTypes.object, // object if using firebase.data
            PropTypes.array // array if using firebase.ordered
        ]),
        firebase: PropTypes.shape({
            set: PropTypes.func.isRequired,
            remove: PropTypes.func.isRequired,
            push: PropTypes.func.isRequired,
            database: PropTypes.oneOfType([PropTypes.object, PropTypes.func])
        }),
        auth: PropTypes.shape({
            uid: PropTypes.string
        })
    }

    state = {
        error: null
    }

    render() {
        const {monitoring} = this.props
        const {error} = this.state

        let timeseries = null;
        let points = []

        if (monitoring) {

            Object.keys(monitoring).forEach(key => {
                let data = monitoring[key]
                const time = new Moment(data.epochInSeconds * 1000).toDate().getTime();
                points.push([
                    time,
                    data.temperature,
                    data.humidity
                ])
            });

            points.sort((a, b) => {
                return a[0] - b[0]
            });

            timeseries = new TimeSeries({
                name: "Monitoring",
                columns: ["time", "temperature", "humidity"],
                points: points
            });
        }


        var temperatures = points.map(point => {
            return point[1];
        });
        var maxTemperature = Math.max.apply(null, temperatures);
        var minTemperature = Math.min.apply(null, temperatures);
        var temperature = temperatures.slice(-1)[0];

        var humidities = points.map(point => {
            return point[2];
        });
        var maxHumidity = Math.max.apply(null, humidities);
        var minHumidity = Math.min.apply(null, humidities);
        var humidity = humidities.slice(-1)[0];

        const symbolContainerStyle = {
            display: 'flex',
            alignItems: 'right',
            marginBottom: '20px'
        };

        const symbolStyle = {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            userSelect: 'none',
            textAlign: 'center',
            height: 200,
            width: 200,
            marginLeft: '10px',
        };

        const symbolTitleStyle = {
            fontSize: '2em',
            marginBottom: '10px',
        };

        const symbolMinMaxStyle = {
            color: 'gray',
            fontSize: '0.8em',
        };

        return (
            <div
                style={{color: Theme.palette.primary2Color}}>
                {error ? (
                    <Snackbar
                        open={!!error}
                        message={error}
                        autoHideDuration={4000}
                        onRequestClose={() => this.setState({error: null})}
                    />
                ) : null}
                <div>
                    {
                        (!isLoaded(monitoring) || timeseries == null) ? (
                            <CircularProgress />
                        ) : (
                            <div>
                                <div style={symbolContainerStyle}>
                                    <Paper style={symbolStyle}>
                                        <div>
                                            <div style={symbolTitleStyle}>{temperature} °C</div>
                                            <div style={symbolMinMaxStyle}>Min: {minTemperature} °C</div>
                                            <div style={symbolMinMaxStyle}>Max: {maxTemperature} °C</div>
                                        </div>
                                    </Paper>
                                    <Paper style={symbolStyle}>
                                        <div>
                                            <div style={symbolTitleStyle}>{humidity} %</div>
                                            <div style={symbolMinMaxStyle}>Min: {minHumidity} %</div>
                                            <div style={symbolMinMaxStyle}>Max: {maxHumidity} %</div>
                                        </div>
                                    </Paper>
                                </div>
                                <div>
                                    <h2>
                                        Temperature
                                    </h2>
                                    <ChartContainer
                                        timeRange={timeseries.timerange()}>
                                        <ChartRow height="200">
                                            <YAxis
                                                id="axis1"
                                                label="°C"
                                                min={0}
                                                max={40}
                                                type="linear"/>
                                            <Charts>
                                                <LineChart
                                                    axis="axis1"
                                                    series={timeseries}
                                                    columns={["temperature"]}
                                                />
                                            </Charts>
                                        </ChartRow>
                                    </ChartContainer>
                                </div>
                                <div>
                                    <h2>
                                        Humidity
                                    </h2>
                                    <ChartContainer
                                        timeRange={timeseries.timerange()}>
                                        <ChartRow height="200">
                                            <YAxis
                                                id="axis1"
                                                label="%"
                                                min={1}
                                                max={100}
                                                type="linear"/>
                                            <Charts>
                                                <LineChart
                                                    axis="axis1"
                                                    series={timeseries}
                                                    columns={["humidity"]}
                                                />
                                            </Charts>
                                        </ChartRow>
                                    </ChartContainer>
                                </div>
                            </div>
                        )
                    }
                </div>
            </div>
        )
    }
}

export default compose(
    firebaseConnect([
        {path: 'monitoring', queryParams: ['orderByKey', 'limitToLast=40'], populates} // 100 most recent
    ]),
    connect(({firebase, firebase: {auth, data: {todos}}}) => ({
            auth,
            monitoring: populate(firebase, 'monitoring', populates) // populate monitoring with users data from redux
        })
    )
)(Home)